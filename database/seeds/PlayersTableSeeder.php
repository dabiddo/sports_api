<?php

use Illuminate\Database\Seeder;
use App\Models\Player;

class PlayersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Player::create(['first_name'=>'David', 'last_name'=>'Torres', 'team_id'=> 1]);
        Player::create(['first_name'=>'Paco', 'last_name'=> 'Gonzales', 'team_id'=> 2 ]);
    }
}
